import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import { ConfigurationService } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from '@universis/common';
import { NgPipesModule } from 'ngx-pipes';

import { RegistrationCheckoutComponent } from './registration-checkout.component';
import {TestingConfigurationService} from '../../../test';

describe('RegistrationCheckoutComponent', () => {
    // tslint:disable-next-line:prefer-const
  let component: RegistrationCheckoutComponent;
    // tslint:disable-next-line:prefer-const
  let fixture: ComponentFixture<RegistrationCheckoutComponent>;

  const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
  const currRegSvc = jasmine.createSpyObj('CurrentRegistrationService', ['getLastRegistration', 'getCurrentRegistrationEffectiveStatus',
            'getAvailableClasses', 'getStudyLevel', 'getSpecialties', 'getCurrentSpecialty', 'setSpecialty', 'getCurrentRegistration',
            'saveCurrentRegistration', 'registerForCourse', 'identifierCompare', 'removeCourse', 'getStudentStatus', 'reset',
            'registerSemester', 'getCurrentRegisterAction', 'getRegistrationStatus']);

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ RegistrationCheckoutComponent ],
      imports: [
        RouterTestingModule,
          HttpClientTestingModule,
        TranslateModule.forRoot(),
          MostModule.forRoot({
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }),
        ModalModule.forRoot(),
        ToastrModule.forRoot(),
        SharedModule,
        TooltipModule.forRoot(),
        NgPipesModule
       ],
      providers: [
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          },
          {
              provide: ProfileService,
              useValue: profileSvc
          },
          {
              provide: CurrentRegistrationService,
              useValue: currRegSvc
        },
      ]
    })
    .compileComponents();
  }));


});
